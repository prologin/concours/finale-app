import datetime
import json

from django.db.models import Sum
from django.http import HttpResponseNotFound, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from .models import Contestant, Meal, Score
from .utils import (
    compute_leaderboard,
    current_meal,
    get_contestant_score,
    get_house_int,
)


@require_http_methods(["GET"])
def get_contestant(request, username):
    contestant = get_object_or_404(Contestant, user__username=username)
    house_name = contestant.get_house_display()
    score = (
        Score.objects.filter(contestant=contestant).aggregate(
            total_score=Sum("points")
        )["total_score"]
        or 0
    )

    contestant_info = {
        "username": username
        if username != "gurvan.biguet–kerloch"
        else "gurvan.biguet-kerloch",
        "name": f"{contestant.user.first_name} {contestant.user.last_name}",
        "house": house_name,
        "score": score,
        "activities": contestant.get_score_categories(),  # FIXME
    }
    return JsonResponse(contestant_info, safe=False)


@require_http_methods(["GET"])
def get_contestants(request):
    contestants = Contestant.objects.all()
    response = [
        {
            "username": contestant.user.username
            if contestant.user.username != "gurvan.biguet–kerloch"
            else "gurvan.biguet-kerloch",
            "name": f"{contestant.user.first_name} {contestant.user.last_name}",
            "house": contestant.get_house_display(),
            "score": get_contestant_score(contestant.user.username),
            "activities": contestant.get_score_categories(),
        }
        for contestant in contestants
    ]
    return JsonResponse(response, safe=False)


@require_http_methods(["GET"])
def get_houses(request):
    house_scores = Contestant.objects.values("house").annotate(
        total_score=Sum("score__points")
    )

    # Create a list to store the results
    results = []
    for score in house_scores:
        house = Contestant.House(score["house"]).label
        total_score = score["total_score"]
        result = {
            "name": house,
            "points": total_score if total_score and total_score > 0 else 0,
        }
        results.append(result)

    return JsonResponse(results, safe=False)


@require_http_methods(["GET"])
def get_house_contestants(request, house_name):
    house_id = get_house_int(house_name)
    if not house_id:
        return HttpResponseNotFound()
    contestants = Contestant.objects.filter(house=house_id)
    res = []

    for contestant in contestants:
        username = (
            contestant.user.username
            if contestant.user.username != "gurvan.biguet–kerloch"
            else "gurvan.biguet-kerloch"
        )
        print(username)
        total_points = get_contestant_score(username)
        print(total_points)
        res.append({"username": username, "points": total_points})
        print("appended")

    # Sort the leaderboard based on total points in descending order
    res.sort(key=lambda x: x["points"], reverse=True)

    return JsonResponse(res, safe=False)


@require_http_methods(["GET"])
def leaderboard(request):
    leaderboard_data = compute_leaderboard()
    return JsonResponse(leaderboard_data, safe=False)


@require_http_methods(["GET"])
def get_scores(request, username):
    contestant = get_object_or_404(Contestant, user__username=username)
    scores = Score.objects.filter(contestant=contestant)
    res = []

    for score in scores:
        res.append({"points": score.points, "category": score.category})

    return JsonResponse(res, safe=False)


@csrf_exempt
@require_http_methods(["POST"])
def set_score(request, username):
    print(username)
    data = json.loads(request.body.decode("utf-8"))
    contestant = get_object_or_404(Contestant, user__username=username)
    score = Score.objects.create(
        contestant=contestant,
        points=data["points"],
        category=data["point_type"],
    )
    score.save()
    return JsonResponse(
        {"success": True, "leaderboard": compute_leaderboard()}
    )


@csrf_exempt
@require_http_methods(["POST"])
def meal(request, username):
    today = datetime.datetime.today().day - 18
    meal_type = current_meal()
    try:
        meal = Meal.objects.get(
            user__username=username, day=today, meal_type=meal_type
        )
        if meal.eaten:
            return JsonResponse(1, safe=False)
        else:
            meal.eaten = True
            meal.save()
            return JsonResponse(0, safe=False)
    except Meal.DoesNotExist:
        return JsonResponse(-1, safe=False)


@csrf_exempt
@require_http_methods(["POST"])
def set_house(request, username):
    data = json.loads(request.body.decode("utf-8"))
    contestant = get_object_or_404(Contestant, user__username=username)

    if contestant.house != 0:
        return JsonResponse({"error": "House already set"}, status=400)

    contestant.house = data["house"]
    contestant.save()
    return JsonResponse({"success": True}, safe=False, status=200)


@require_http_methods(["GET"])
def eaten(request):
    today = datetime.datetime.today().day - 18
    meal_type = current_meal()
    try:
        count = Meal.objects.filter(
            day=today, meal_type=meal_type, eaten=True
        ).count()
        orgas_count = Meal.objects.filter(
            user__is_staff=True, day=today, meal_type=meal_type, eaten=True
        ).count()
        contestants_count = Meal.objects.filter(
            user__is_staff=False, day=today, meal_type=meal_type, eaten=True
        ).count()
        return JsonResponse(
            {
                "orgas": orgas_count,
                "contestants": contestants_count,
                "total": count,
            },
            safe=False,
        )
    except Exception:
        return JsonResponse(
            {"orgas": 0, "contestants": 0, "total": 0}, safe=False
        )
