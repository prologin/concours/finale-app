from django.urls import path

from . import views

app_name = "finale"

urlpatterns = [
    path(
        "contestant/<str:username>/",
        views.get_contestant,
        name="get_contestant",
    ),
    path("contestants/", views.get_contestants, name="get_contestants"),
    path("leaderboard/", views.leaderboard, name="leaderboard"),
    path(
        "houses/contestant/<str:username>/",
        views.set_house,
        name="set_house",
    ),
    path(
        "houses/<str:house_name>/",
        views.get_house_contestants,
        name="get_house_contestants",
    ),
    path("houses/", views.get_houses, name="get_houses"),
    path("scores/<str:username>/", views.get_scores, name="get_scores"),
    path(
        "score/<str:username>/", views.set_score, name="set_contestant_score"
    ),
    path("meal/<str:username>/", views.meal, name="meal_set"),
    path("eaten/", views.eaten, name="has_eaten"),
]
