import 'package:finale_app/leaderboard.dart';
import 'package:finale_app/score_adder.dart';
import 'package:finale_app/set_house.dart';
import 'package:finale_app/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyAppState(),
      child: MaterialApp(
        title: 'Namer App',
        theme: ThemeData(
          useMaterial3: true,
          // TODO: change that colorscheme
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueAccent),
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyAppState extends ChangeNotifier {
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    Widget page;
    switch (selectedIndex) {
      case 0:
        page = const MainLeaderboard();
        break;
      case 1:
        page = const HouseLeaderboard();
        break;
      case 2:
        page = const ScoreScannerPage();
        break;
      case 3:
        page = HouseSet();
        break;
      case 4:
        page = QRCodeScanner(
          onScan: (value) {
            // TODO: Get contestant info
          },
        );
        break;
      default:
        throw UnimplementedError('No widget for page $selectedIndex');
    }

    final theme = Theme.of(context);
    final headerText = theme.textTheme.displaySmall!.copyWith(
        color: theme.colorScheme.onPrimary
    );

    return Scaffold(
      body: page,
      appBar: AppBar(
        title: const Text('')
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: theme.colorScheme.primary,
              ),
              child: Text('Menu', style: headerText),
            ),
            ListTile(
              title: const Text('Leaderboard général'),
              onTap: () {
                setState(() {
                  selectedIndex = 0;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Leaderboard des Maisons'),
              onTap: () {
                setState(() {
                  selectedIndex = 1;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Ajouter des points'),
              onTap: () {
                setState(() {
                  selectedIndex = 2;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Définir la maison'),
              onTap: () {
                setState(() {
                  selectedIndex = 3;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Annuler un scan'),
              onTap: () {
                setState(() {
                  selectedIndex = 4;
                });
                Navigator.pop(context);
              },
            ),
          ],
        )
      ),
    );
  }
}
