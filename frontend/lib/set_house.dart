import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:finale_app/utils.dart';
import 'package:http/http.dart';

class HouseSet extends StatelessWidget
{
  const HouseSet({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: QRCodeScanner(
          onScan: (value) {
            Navigator.push(context, MaterialPageRoute<void>(
                builder: (context) => HouseListPage(contestant: value)
            ));
          },
        )
    );
  }
}

class HouseListPage extends StatefulWidget
{
  final String? contestant;

  const HouseListPage({super.key, required this.contestant});

  @override
  State<HouseListPage> createState() => _HouseListPageState();
}

class _HouseListPageState extends State<HouseListPage> {
  final controller = TextEditingController();
  String? selectedType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('')
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  HouseButton(text: "Dvalin", contestant: widget.contestant, color: const Color.fromRGBO(37, 150, 178, 1),), // blue house
                  HouseButton(text: "Durathror", contestant: widget.contestant, color: const Color.fromRGBO(217, 62, 48, 1),), // red house
                  HouseButton(text: "Dain", contestant: widget.contestant, color: const Color.fromRGBO(114, 0, 88, 1),), // purple house
                  HouseButton(text: "Duneyrr", contestant: widget.contestant, color: const Color.fromRGBO(0, 110, 91, 1),), // green house
                ]
            )
          ]
      ),
    );
  }
}

class HouseButton extends StatelessWidget
{
  final houseNames = [ // TODO: Change the houses name
    "Dvalin",
    "Durathror",
    "Dain",
    "Duneyrr"
  ];

  final String text;
  final contestant;
  final color;

  HouseButton({super.key, required this.text, required this.contestant, required this.color});

  Future<Response> setHouse(int house) async {
    // NOTE(daif): for some reason, flutter removes the quotes after a toString
    var body = { "house": house, }.toString();
    body = body.replaceAll('{', '{"');
    body = body.replaceAll(': ', '": "');
    body = body.replaceAll(', ', '", "');
    body = body.replaceAll('}', '"}');
    print(body);

    return post(
        Uri.parse("$baseUrl/houses/contestant/$contestant/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body
    );
  }

  @override
  Widget build(BuildContext context)
  {
    final theme = Theme.of(context);

    return Container(
          padding: const EdgeInsets.all(20),
          child: FilledButton(
              onPressed: () {
                setHouse(houseNames.indexOf(text) + 1).then((response) {
                  if (response.statusCode == 200) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text("Maison modifiée"),
                          backgroundColor: Colors.green,
                        )
                    );
                    Navigator.pop(context);
                  }
                  else {
                    var data = jsonDecode(response.body);
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("Error: ${data["error"]}"),
                          backgroundColor: Colors.deepOrangeAccent,
                        )
                    );
                  }
                });
              },
              style: FilledButton.styleFrom(
                  backgroundColor: color,
                  foregroundColor: theme.colorScheme.onPrimary,
                  padding: const EdgeInsets.all(20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  textStyle: theme.textTheme.titleLarge
              ),
              child: Text(text)
          )
      );
  }
}
