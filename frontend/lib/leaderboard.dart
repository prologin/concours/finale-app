import 'dart:convert';

import 'package:finale_app/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';

class LeaderboardItem extends StatelessWidget {
  const LeaderboardItem({
    super.key,
    required this.name,
    required this.score,
    required this.rank,
    required this.creator,
  });

  final creator;
  final String name;
  final int score;
  final int rank;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    var crownColor = const Color.fromRGBO(0, 0, 0, 0);
    if (rank == 1)
      crownColor = const Color.fromRGBO(229, 184, 11, 1);
    else if (rank == 2)
      crownColor = const Color.fromRGBO(192, 192, 192, 1);
    else if (rank == 3)
      crownColor = const Color.fromRGBO(196, 156, 72, 1);

    Widget placementWidget = Icon(FontAwesomeIcons.crown, color: crownColor);

    if (rank > 3) {
      placementWidget = Text(rank.toString());
    }

    return Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1,
              color: theme.colorScheme.primary
            )
          )
        ),
        child: InkWell(
          onTap: () => {
            Navigator.push(context, MaterialPageRoute<void>(
              builder: (context) => creator(name, rank, score)
            ))
          },
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: [
                Expanded(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                            right: 10
                          ),
                          child: CircleAvatar(
                            backgroundColor: (rank > 3) ? theme.colorScheme.primaryContainer : theme.colorScheme.background,
                            child: placementWidget,
                          ),
                        ),
                        Text(name),
                      ],
                    )
                ),
                Text(score.toString())
              ],
            )
          )
        )
    );
  }
}

class UserList extends StatefulWidget {
  final String endpoint;
  final String nameKey;
  final String scoreKey;
  final bool hasRank;
  final detailsCreator;

  const UserList({
    super.key,
    required this.endpoint,
    required this.nameKey,
    required this.scoreKey,
    required this.hasRank,
    required this.detailsCreator,
  });

  @override
  State<UserList> createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  late var userList = [];

  Future<String> getContestants(String url) async {
    var response = await get(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }
    );
    return response.body;
  }

  @override
  void initState() {
    super.initState();
    getContestants("$baseUrl/${widget.endpoint}").then((value) => {
      userList = List.from(jsonDecode(value)),
      if (!widget.hasRank)
        userList.sort((a, b) => a[widget.scoreKey] > b[widget.scoreKey] ? -1 : 1),
      setState(() {})
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: userList.length,
        itemBuilder: (context, index) {
          final item = userList[index];

          return LeaderboardItem(
            name: item[widget.nameKey],
            score: item[widget.scoreKey],
            rank: (widget.hasRank) ? item["rank"] : index + 1,
            creator: widget.detailsCreator,
          );
        },
      ),
    );
  }
}

class MainLeaderboard extends StatelessWidget {
  const MainLeaderboard({super.key});

  ContestantDetails createDetails(String name, int rank, int score) {
    return ContestantDetails(name: name, rank: rank, score: score,);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: UserList(
        endpoint: "leaderboard/",
        nameKey: "username",
        scoreKey: "score",
        hasRank: true,
        detailsCreator: createDetails,
      ),
    );
  }
}
class HouseLeaderboard extends StatelessWidget {
  const HouseLeaderboard({super.key});

  HouseDetails createDetails(String name, int rank, int score) {
    return HouseDetails(name: name, rank: rank, score: score);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: UserList(
        endpoint: "houses/",
        nameKey: "name",
        scoreKey: "points",
        hasRank: false,
        detailsCreator: createDetails,
      ),
    );
  }
}

class ContestantDetails extends StatefulWidget {
  final name;
  final score;
  final rank;

  const ContestantDetails({super.key, required this.name, required this.rank, required this.score});

  @override
  State<ContestantDetails> createState() => _ContestantDetailsState();
}

class _ContestantDetailsState extends State<ContestantDetails> {
  late var scoreList = [];

  Future<String> getScores() async {
    var response = await get(
        Uri.parse("$baseUrl/scores/${widget.name}"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }
    );
    return response.body;
  }

  @override
  void initState() {
    super.initState();
    getScores().then((value) => {
      scoreList = List.from(jsonDecode(value)),
      setState(() {})
    });
  }

  @override
  Widget build(BuildContext context)
  {
    final theme = Theme.of(context);
    var crownColor = theme.colorScheme.primaryContainer;
    if (widget.rank == 1)
      crownColor = const Color.fromRGBO(229, 184, 11, 1);
    else if (widget.rank == 2)
      crownColor = const Color.fromRGBO(192, 192, 192, 1);
    else if (widget.rank == 3)
      crownColor = const Color.fromRGBO(196, 156, 72, 1);

    Widget placementWidget = Text(widget.rank.toString(), style: theme.textTheme.displayLarge,);

    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 40,
          ),
          Text(widget.name, style: theme.textTheme.displayMedium,),
          Padding(
            padding: const EdgeInsets.all(30),
            child: CircleAvatar(
              minRadius: theme.textTheme.displayLarge!.fontSize!,
              backgroundColor: crownColor,
              child: placementWidget,
            ),
          ),
          Text("${widget.score} points", style: theme.textTheme.displaySmall,),
          const SizedBox(
            height: 40,
          ),
          Flexible(
            child: ListView.builder(
              itemCount: scoreList.length,
              itemBuilder: (context, index) {
                final item = scoreList[index];
                return ListTile(
                  title: Row(
                    children: [
                      Expanded(child: Text(animationCategory[item["category"]])),
                      Text(item["points"].toString())
                    ],
                  ),
                );
              },
            )
          )
        ],
      )
    );
  }
}

class HouseDetails extends StatefulWidget {
  final name;
  final score;
  final rank;

  const HouseDetails({super.key, required this.name, required this.rank, required this.score});

  @override
  State<HouseDetails> createState() => _HouseDetailsState();
}

class _HouseDetailsState extends State<HouseDetails> {
  late var contestantList = [];

  ContestantDetails createDetails(String name, int rank, int score) {
    return ContestantDetails(name: name, rank: rank, score: score,);
  }

  Future<String> getScores() async {
    var response = await get(
        Uri.parse("$baseUrl/scores/${widget.name}"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }
    );
    return response.body;
  }

  @override
  void initState() {
    super.initState();
    getScores().then((value) => {
      contestantList = List.from(jsonDecode(value)),
      setState(() {})
    });
  }

  @override
  Widget build(BuildContext context)
  {
    final theme = Theme.of(context);
    var crownColor = theme.colorScheme.primaryContainer;
    if (widget.rank == 1)
      crownColor = const Color.fromRGBO(229, 184, 11, 1);
    else if (widget.rank == 2)
      crownColor = const Color.fromRGBO(192, 192, 192, 1);
    else if (widget.rank == 3)
      crownColor = const Color.fromRGBO(196, 156, 72, 1);

    Widget placementWidget = Text(widget.rank.toString(), style: theme.textTheme.displayLarge,);

    return Scaffold(
        appBar: AppBar(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 40,
            ),
            Text(widget.name, style: theme.textTheme.displayMedium,),
            Padding(
              padding: const EdgeInsets.all(30),
              child: CircleAvatar(
                minRadius: theme.textTheme.displayLarge!.fontSize!,
                backgroundColor: crownColor,
                child: placementWidget,
              ),
            ),
            Text("${widget.score} points", style: theme.textTheme.displaySmall,),
            const SizedBox(
              height: 40,
            ),
            Flexible(
              child: Container(
                child: UserList(
                  endpoint: "houses/${widget.name}",
                  nameKey: "username",
                  scoreKey: "points",
                  hasRank: false,
                  detailsCreator: createDetails,
                ),
              )
            )
          ],
        )
    );
  }
}
