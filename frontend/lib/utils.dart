import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

// TODO: Change to backend endpoint
String baseUrl = "http://10.0.2.2:8000/finale";

final animationCategory = [
  "Kermesse",
  "Chaises-Musicales",
  "Gonflable",
  "Blindtest",
  "Karaoke",
  "Loup-Garou",
  "Maquillage",
  "Chasse-au-tresor",
  "Chasse-aux-Orgas",
  "Jeux-Videos",
  "Question-Pour-un-Prolo",
  "Misc"
];

class SelectionWidget extends StatefulWidget {
  final List<String> selection;

  const SelectionWidget({super.key, required this.selection});

  @override
  State<SelectionWidget> createState() => _SelectionWidgetState();
}

class _SelectionWidgetState extends State<SelectionWidget> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final indexes = Iterable<int>.generate(widget.selection.length).toList();

    final theme = Theme.of(context);
    final colorSelected = ElevatedButton.styleFrom(
      backgroundColor: theme.colorScheme.primaryContainer,
      foregroundColor: theme.colorScheme.onPrimaryContainer,
    );
    final colorClassic = theme.textButtonTheme.style;

    return Center(
      child: Wrap(
        spacing: 15,
        alignment: WrapAlignment.spaceEvenly,
        children: indexes.map<Widget>((int i) {
          return ElevatedButton(
            onPressed: () {
              setState(() {
                selectedIndex = i;
              });
            },
            style: selectedIndex == i ? colorSelected : colorClassic,
            child: Text(widget.selection[i])
          );
        }).toList(),
      )
    );
  }
}

class QRCodeScanner extends StatefulWidget {
  final ValueSetter<String?> onScan;

  const QRCodeScanner({super.key, required this.onScan});

  @override
  State<QRCodeScanner> createState() => _QRCodeScannerState();
}

class _QRCodeScannerState extends State<QRCodeScanner> with WidgetsBindingObserver {
  final controller = MobileScannerController(
    detectionSpeed: DetectionSpeed.noDuplicates
  );

  Barcode? barcode;
  StreamSubscription<Object?>? _subscription;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state)
  {
    super.didChangeAppLifecycleState(state);

    switch (state)
    {
      case AppLifecycleState.detached:
      case AppLifecycleState.hidden:
      case AppLifecycleState.paused:
        break;

      case AppLifecycleState.resumed:
        _subscription = controller.barcodes.listen(handleBarcode);

        unawaited(controller.start());

      case AppLifecycleState.inactive:
        unawaited(_subscription?.cancel());
        _subscription = null;
        unawaited(controller.stop());
    }
  }

  @override
  void initState() {
    controller.stop();
    Future.delayed(const Duration(milliseconds: 1000), () {
      WidgetsBinding.instance.addObserver(this);
      _subscription = controller.barcodes.listen(handleBarcode);
      controller.start();
    });
    super.initState();
  }

  @override
  Future<void> dispose() async {
    WidgetsBinding.instance.removeObserver(this);
    unawaited(_subscription?.cancel());
    _subscription = null;
    super.dispose();
    await controller.dispose();
  }

  void handleBarcode(BarcodeCapture barcodes)
  {
    if (mounted) {
      setState(() {
        barcode = barcodes.barcodes.firstOrNull;
      });

      widget.onScan(barcode?.displayValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MobileScanner(
        controller: controller,
      )
    );
  }
}
