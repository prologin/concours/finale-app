import 'package:finale_app/utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class ScoreScannerPage extends StatelessWidget
{
  const ScoreScannerPage({super.key});

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          body: QRCodeScanner(
          onScan: (value) {
            Navigator.push(context, MaterialPageRoute<void>(
                builder: (context) => ScoreSendPage(contestant: value)
            ));
          },
        )
      );
  }
}

class ScoreSendPage extends StatefulWidget
{
  final String? contestant;

  const ScoreSendPage({super.key, required this.contestant});

  @override
  State<ScoreSendPage> createState() => _ScoreSendPageState();
}

class _ScoreSendPageState extends State<ScoreSendPage> {
  final controller = TextEditingController();
  String? selectedType;

  Future<Response> setScore(String url, String points, int pointType) {
    return post(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: {
          "points": points,
          "point_type": animationCategory,
        }.toString()
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
          title: const Text('')
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                padding: const EdgeInsets.all(30),
                child: DropdownMenu<String>(
                  controller: controller,
                  enableFilter: true,
                  requestFocusOnTap: true,
                  leadingIcon: const Icon(Icons.search),
                  label: const Text('Activité'),
                  onSelected: (String? selection) {
                    setState(() {
                      selectedType = selection;
                    });
                  },
                  dropdownMenuEntries:
                  animationCategory.map((e) =>
                      DropdownMenuEntry(value: e, label: e)
                  ).toList(),
                )
            ),
            Container(
              padding: const EdgeInsets.only(
                  left: 40,
                  right: 40,
                  bottom: 30
              ),
              child: const TextField(
                decoration: InputDecoration(labelText: "Score"),
                keyboardType: TextInputType.number,
              ),
            ),
            const SizedBox(height: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: const EdgeInsets.all(40),
                  child: FilledButton(
                      onPressed: () {
                        setScore("$baseUrl/score/${widget.contestant}/", controller.value.text, animationCategory.indexOf(selectedType!)).then((response) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text("Points ajoutés"),
                                backgroundColor: Colors.green,
                              )
                          );
                        });
                      },
                      style: FilledButton.styleFrom(
                        backgroundColor: theme.colorScheme.primary,
                        foregroundColor: theme.colorScheme.onPrimary,
                        padding: const EdgeInsets.all(20),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        textStyle: theme.textTheme.titleLarge
                      ),
                      child: const Text('Ajouter')
                  )
                )
              ]
            )
          ]
      ),
    );
  }
}
